package com.gitlab.fduermael.demoscala.math

import Complex._

object ComplexTest extends App {

  val z1 = new Complex(1, 2)
  val z2 = new Complex(1, -3)

  val z3 = 2 + 3 * i;

  val cp: List[Complex] = List(z1, z2, z3)

  val sum = cp.reduce((x, y) => x + y)

  val prod = cp.reduce((x, y) => x * y)

  val result = sum / prod;

  println(result)

  println(!result);

}
