package com.gitlab.fduermael.demoscala

object MapReduceTest extends App {

  val l : List[String] = List ("aa","abc","ac")

  println(l)

  val lens =  l.map( s => s.length)

  println(lens)

  val lc =  l.map( s => s.contains("c"))

  println(lc)

  val resultOr = lc.reduce( (x,y) => x||y)

  println(resultOr)

  val resultAnd = lc.reduce( (x,y) => x&&y)

  println(resultAnd)

  //l.map{ (s) =>  s.split(" ").map((s) => (s, 1)) }.reduceByKey(0){  (count: Int, partialCount: Int) => count + partialcount }

}
