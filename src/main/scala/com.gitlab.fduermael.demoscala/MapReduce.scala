package com.gitlab.fduermael.demoscala


// from learning spark streaming
// cf. also https://gist.github.com/ueshin/740567


object MapReduce extends App {

  def map[T,U](l: List[T])(f: T => U): List[U] = for (i <- l) yield f(i)

  def reduce[T](l: List[T])(neutral: T)(op: (T, T) => T): T = l match {
    case Nil => neutral
    case h :: t => reduce(t)(op(neutral, h))(op)
  }



}