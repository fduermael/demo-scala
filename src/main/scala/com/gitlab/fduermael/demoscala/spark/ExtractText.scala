package com.gitlab.fduermael.demoscala.spark

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.input.PortableDataStream
import org.apache.tika.metadata._
import org.apache.tika.parser._
import org.apache.tika.sax.WriteOutContentHandler
import java.io._

import com.johnsnowlabs.nlp.pretrained.PretrainedPipeline
import org.apache.spark.sql.SparkSession

import scala.collection.mutable.ListBuffer


object ExtractText extends App {
  System.setProperty("hadoop.home.dir", "c:/winutils")

  def tikaExtract(a: (String, PortableDataStream)): String = {
    val file: File = new File(a._1.drop(5))
    val myparser: AutoDetectParser = new AutoDetectParser()
    val stream: InputStream = new FileInputStream(file)
    val handler: WriteOutContentHandler = new WriteOutContentHandler(-1)
    val metadata: Metadata = new Metadata()
    val context: ParseContext = new ParseContext()
    myparser.parse(stream, handler, metadata, context)
    stream.close
    handler.toString()
  }

  //val filesPath = "C:\\Users\\frup43047\\Projects\\github.com\\BibleEtScienceDiffusion\\GenealogyTools\\src\\main\\resources\\corpus\\Anonymous\\Jasher\\*"
  val filesPath = "C:\\Users\\frup43047\\Projects\\github.com\\BibleEtScienceDiffusion\\GenealogyTools\\src\\main\\resources\\corpus\\Bible_KJV\\Genesis\\*"
  //val conf = new SparkConf().setAppName("TikaFileParser").setMaster("local[*]").set("spark.executor.memory", "1g");
  //val sc = new SparkContext(conf)

  val spark: SparkSession = SparkSession
    .builder()
    .appName("test")
    .master("local[*]")
    .config("spark.driver.memory", "1G")
    .config("spark.kryoserializer.buffer.max","200M")
    .config("spark.serializer","org.apache.spark.serializer.KryoSerializer")
    .getOrCreate()

  spark.sparkContext.setLogLevel("WARN")

  import spark.implicits._

  val fileData = spark.sparkContext.binaryFiles(filesPath)
  var index = 1;
  var list = ListBuffer[Tuple2[Integer, String]]()
  fileData.foreach(x => {
    list += Tuple2[Integer, String](index, tikaExtract(x))
    index += 1
  })
  //list.foreach(x => println(x))

  val chapters  = list.toDS.toDF( "_id", "text")


  val datetimeMatcherPipeline = PretrainedPipeline("match_datetime","en").model

  val annotation = datetimeMatcherPipeline.transform(chapters)

  annotation.show()
}



