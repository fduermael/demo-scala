package com.gitlab.fduermael.demoscala.spark

import com.johnsnowlabs.nlp.pretrained.PretrainedPipeline
import org.apache.spark.sql.SparkSession

/**
 * http://cedric.cnam.fr/vertigo/Cours/RCP216/tpNlp.html
 */

object DependencyParseDocument extends App {
  System.setProperty("hadoop.home.dir", "c:/winutils")
  val spark: SparkSession = SparkSession
    .builder()
    .appName("test")
    .master("local[*]")
    .config("spark.driver.memory", "4G")
    .config("spark.kryoserializer.buffer.max","200M")
    .config("spark.serializer","org.apache.spark.serializer.KryoSerializer")
    .getOrCreate()

  val texteFr = spark.read.textFile("src/main/resources/texteEn.txt").toDF("text")

  val datetimeMatcherPipeline = PretrainedPipeline("dependency_parse","en").model

  val annotation = datetimeMatcherPipeline.transform(texteFr)

  annotation.show()

}
