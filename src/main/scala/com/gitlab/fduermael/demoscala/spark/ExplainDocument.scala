package com.gitlab.fduermael.demoscala.spark

import com.johnsnowlabs.nlp.base.{DocumentAssembler, Finisher}
import com.johnsnowlabs.nlp.pretrained.PretrainedPipeline
import org.apache.spark.ml.Pipeline
import org.apache.spark.sql.SparkSession

/**
 * http://cedric.cnam.fr/vertigo/Cours/RCP216/tpNlp.html
 */

object ExplainDocument extends App {
  System.setProperty("hadoop.home.dir", "c:/winutils")
  val spark: SparkSession = SparkSession
    .builder()
    .appName("test")
    .master("local[*]")
    .config("spark.driver.memory", "4G")
    .config("spark.kryoserializer.buffer.max","200M")
    .config("spark.serializer","org.apache.spark.serializer.KryoSerializer")
    .getOrCreate()

  val texteFr = spark.read.textFile("src/main/resources/texteFr.txt").toDF("text")


  val explainPipelineModel = PretrainedPipeline("explain_document_md","fr").model
  val finisherExplainFr = new Finisher().setInputCols("token", "lemma", "pos", "ner", "entities")
  val pipelineExplainFr = new Pipeline().setStages(Array(explainPipelineModel,finisherExplainFr))

  val modelExplainFr = pipelineExplainFr.fit(texteFr)
  val annoteTexteFr = modelExplainFr.transform(texteFr)

  annoteTexteFr.show()
  annoteTexteFr.select("finished_token").show(false)
  annoteTexteFr.select("finished_lemma").show(false)
  annoteTexteFr.select("finished_pos").show(false)
  annoteTexteFr.select("finished_ner").show(false)
  annoteTexteFr.select("finished_entities").show(false)

}
